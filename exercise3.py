# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 20:20:16 2020

@author: choud
"""

import scipy.io
import pandas as pd
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from scipy.cluster.vq import whiten
from sklearn.decomposition import FastICA

mat=scipy.io.loadmat('000.mat')
X = mat['val']
rate = 360

num = X.shape[2]
duration = num/rate
time = np.linspace(0,duration,num)

#a=X[:,0,:]
ns=np.linspace(0,1500,1)
fig, ax = plt.subplots(4, 1, figsize=[18, 5], sharex=True)
ax[0].plot(time, X[0,0,:])
ax[0].set_xlabel('Sample number', fontsize=20)
ax[0].set_title('Mixed signals from Electrode 1', fontsize=25)

ax[1].plot(time, X[0,1,:])
ax[1].tick_params(labelsize=12)
ax[1].set_title('Mixed signals from Electrode 2', fontsize=25)
ax[1].set_xlabel('Sample number', fontsize=20)

ax[2].plot(time, X[0,2,:])
ax[2].tick_params(labelsize=12)
ax[2].set_title('Mixed signals from Electrode 3', fontsize=25)
ax[2].set_xlabel('Sample number', fontsize=20)

ax[3].plot(time, X[0,3,:])
ax[3].tick_params(labelsize=12)
ax[3].set_title('Mixed signals from Electrode 4', fontsize=25)
ax[3].set_xlabel('Sample number', fontsize=20)
plt.tight_layout()
plt.show()



ica = FastICA(n_components=4)
S = ica.fit_transform(X[0,:,:].T)  # Reconstruct signals
plt.plot(S)

plt.subplot(221)
plt.plot(S[:,0])
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.title('Signal 1')
plt.subplot(222)
plt.plot(S[:,1])
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.title('Signal 2')
plt.subplot(223)
plt.plot(S[:,2])
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.title('Signal 3')
plt.subplot(224)
plt.plot(S[:,3])
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.title('Signal 4')
plt.tight_layout()
plt.show()


max_amp0=np.max(S[:,0])
max_amp1=np.max(S[:,1])
max_amp2=np.max(S[:,2])
max_amp3=np.max(S[:,3])

min_amp0=np.min(S[:,0])
min_amp1=np.min(S[:,1])
min_amp2=np.min(S[:,2])
min_amp3=np.min(S[:,3])

act_sgn=[]
j=[]
max_amp=[max_amp0,max_amp1,max_amp2,max_amp3]
min_amp=[min_amp0,min_amp1,min_amp2,min_amp3]
for i in range (len(max_amp)):
    if(max_amp[i]>0.1):
        j.append(i)
        act_sgn.append(max_amp[i])
        act_sgn.append(max_amp[i])
        
        
for i in range (len(max_amp)):
    if(min_amp[i]<-0.1):
        j.append(i)
        act_sgn.append(min_amp[i])
        act_sgn.append(min_amp[i])
        
        
print(act_sgn)
plt.subplot(211)
plt.plot(S[:,j[0]])
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.title('Relevant Signal 1')
plt.subplot(212)
plt.plot(S[:,j[1]])
plt.xlabel('Time')
plt.ylabel('Amplitude')
plt.title('Relevant Signal 2')
plt.tight_layout()
plt.show()

count0=0
count1=0
# Fine tune the threshold values according to the plots of the relevant signals
for i in range(len(S[:,j[0]])):
    if((S[i,j[0]]>0.09) or (S[i,j[0]]<=-0.09)):
        count0=count0+1
            
for i in range(len(S[:,j[1]])):
    if((S[i,j[1]]>0.09) or (S[i,j[1]]<-0.09)):
        count1=count1+1

if (count0>count1):
    plt.subplot(211)
    plt.plot(S[:,j[0]])
    plt.xlabel('Time')
    plt.ylabel('Heartbeat')
    plt.title('Fetus Heart beat')
    plt.subplot(212)
    plt.plot(S[:,j[1]])
    plt.xlabel('Time')
    plt.ylabel('Heartbeat')
    plt.title("Mother's Heart beat")
    plt.tight_layout()
    plt.show()
    
else:
    plt.subplot(211)
    plt.plot(S[:,j[1]])
    plt.xlabel('Time')
    plt.ylabel('Heartbeat')
    plt.title('Fetus Heart beat')
    plt.subplot(212)
    plt.plot(S[:,j[0]])
    plt.xlabel('Time')
    plt.ylabel('Heartbeat')
    plt.title("Mother's Heart beat")
    plt.tight_layout()
    plt.show()



